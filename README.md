# Advertising Service

#### Requisites
* [Install Git](https://git-scm.com/downloads) 
* [Install OpenJDK 8](https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html)
* [Install MySQL 7 or above](https://dev.mysql.com/downloads/installer/)
* [Install Maven 3](https://maven.apache.org/download.cgi)

#### How to run

* Open your terminal
* execute the next command to clone the app:
``` shell
    git clone https://
``` 
    
* navigate into the project directory : 
``` shell
    cd advertising-service 
```
* create the application-prod.properties file in resources folder  
``` shell
    cd src/main/resoruces
```
* Paste the followings lines in that file and update the username and password in order to connect to MySQL database : 
``` shell
    spring.datasource.username=mysql_username
    spring.datasource.password=mysql_password
```

* run the next command  from the application directory to start the application :
``` shell
    mvn spring-boot:run
```
   
* API documentation : http://localhost:8000/swagger-ui.html
