insert into listing (vehicle, price, state,  created_at, updated_at, dealer_id)
values ('Toyota Verso', 100, 'draft', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1);

insert into listing (vehicle, price, state,  created_at, updated_at, dealer_id)
values ('Toyota Carina', 300, 'draft', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1);

insert into listing (vehicle, price, state,  created_at, updated_at, dealer_id)
values ('Mercedes C200', 500, 'published', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1);