package com.dfv.advertising.services;

import com.dfv.advertising.commons.ListingState;
import com.dfv.advertising.dto.request.ActionDtoRequest;
import com.dfv.advertising.dto.request.ListingDtoRequest;
import com.dfv.advertising.entities.DealerEntity;
import com.dfv.advertising.entities.ListingEntity;
import com.dfv.advertising.exceptions.ResourceNotfoundException;
import com.dfv.advertising.mappers.ListingMapperImpl;
import com.dfv.advertising.repositories.ListingRepository;
import com.dfv.advertising.serviceimpl.ListingServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ListingServiceTest{

    @Mock
    private ListingRepository listingRepository;


    @Mock
    private DealerService dealerService;

    @Mock
    private ListingMapperImpl mapper;

    @InjectMocks
    private ListingServiceImpl listingService;

    private DealerEntity dealerEntity;

    @BeforeEach
    public void setup(){
        dealerEntity = DealerEntity.builder()
                .id(1L)
                .name("Vicky Foukou")
                .tierLimit(2)
                .createdAt(LocalDateTime.now()).build();
    }


    @Test
    public void createListingTest() throws ResourceNotfoundException {

        when(dealerService.getDealerEntity(Mockito.any(Long.class))).thenReturn(dealerEntity);

        ListingDtoRequest listingDtoRequest = new ListingDtoRequest();
        listingDtoRequest.setDealerId(1L);
        listingDtoRequest.setPrice(200);
        listingDtoRequest.setVehicle("Toyota Verso");

        ListingEntity listingEntity = ListingEntity.builder()
                .price(listingDtoRequest.getPrice())
                .vehicle(listingDtoRequest.getVehicle())
                .dealerEntity(dealerEntity)
                .build();

        when(mapper.dtoToEntity(Mockito.any(ListingDtoRequest.class))).thenReturn(listingEntity);

        ListingEntity savedListingEntity = listingService.createListing(listingDtoRequest);
        assertThat(savedListingEntity).isNotNull();
    }

    @Test
    public void updateListingTest() throws ResourceNotfoundException {
        long listingId = 1L;
        ListingEntity listingEntity = ListingEntity.builder()
                .id(listingId)
                .vehicle("Toyota VVerso")
                .price(200)
                .state(ListingState.draft)
                .dealerEntity(dealerEntity)
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build();

        when(listingRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(listingEntity));

        ListingDtoRequest listingDtoRequest = new ListingDtoRequest();
        listingDtoRequest.setVehicle("Toyota RAV 4");
        listingDtoRequest.setPrice(400);
        listingDtoRequest.setDealerId(dealerEntity.getId());

        ListingEntity updatedListingEntity = listingService.update(listingId, listingDtoRequest);

        assertThat(updatedListingEntity).isNotNull();
        assertThat(updatedListingEntity.getVehicle()).isEqualTo(listingDtoRequest.getVehicle());
        assertThat(updatedListingEntity.getPrice()).isEqualTo(listingDtoRequest.getPrice());
        assertThat(updatedListingEntity.getDealerEntity().getId()).isEqualTo(listingDtoRequest.getDealerId());
    }


    @Test
    public void publishListingWhenTheDealerTierLimitIsReachedWillReturnTrueTest() throws ResourceNotfoundException {
        long listingId = 2;
        ListingEntity listingEntity = ListingEntity.builder()
                .id(listingId)
                .vehicle("Toyota Carina")
                .price(200)
                .state(ListingState.draft)
                .dealerEntity(dealerEntity)
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build();


        when(listingRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(listingEntity));
        when(listingRepository.getTotalOfPublishedListings(Mockito.anyLong())).thenReturn(2L);

        ListingEntity oldListingEntity =  ListingEntity.builder()
                .id(1L)
                .vehicle("Toyota Verso")
                .price(100)
                .state(ListingState.published)
                .dealerEntity(dealerEntity)
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build();

        when(listingRepository.findFirstByDealerEntityAndStateOrderByCreatedAtAsc(Mockito.any(DealerEntity.class), Mockito.any(ListingState.class)))
                .thenReturn(oldListingEntity);
        when(listingRepository.save(Mockito.any(ListingEntity.class))).thenReturn(listingEntity);
        ActionDtoRequest actionDtoRequest = new ActionDtoRequest();
        actionDtoRequest.setForce(true);
        boolean response = listingService.publishListing(listingId, actionDtoRequest);

        assertThat(response).isTrue();
    }

    @Test
    public void publishListingWhenTheDealerTierLimitIsReachedWillReturnFalseTest() throws ResourceNotfoundException {
        long listingId = 2;
        ListingEntity listingEntity = ListingEntity.builder()
                .id(listingId)
                .vehicle("Toyota Carina")
                .price(200)
                .state(ListingState.draft)
                .dealerEntity(dealerEntity)
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build();


        when(listingRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(listingEntity));
        when(listingRepository.getTotalOfPublishedListings(Mockito.anyLong())).thenReturn(2L);
        boolean response = listingService.publishListing(listingId, new ActionDtoRequest());
        assertThat(response).isFalse();
    }

    @Test
    public void publishListingTest() throws ResourceNotfoundException {
        long listingId = 1;
        ListingEntity listingEntity = ListingEntity.builder()
                .id(listingId)
                .vehicle("Toyota VVerso")
                .price(200)
                .state(ListingState.draft)
                .dealerEntity(dealerEntity)
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build();


        when(listingRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(listingEntity));
        when(listingRepository.getTotalOfPublishedListings(Mockito.anyLong())).thenReturn(0L);
        when(listingRepository.save(Mockito.any(ListingEntity.class))).thenReturn(listingEntity);

        boolean response = listingService.publishListing(listingId, new ActionDtoRequest());

        assertThat(response).isTrue();
    }

    @Test
    public void unPublishListingTest() throws ResourceNotfoundException {
        long listingId = 1;
        ListingEntity listingEntity = ListingEntity.builder()
                .id(listingId)
                .vehicle("Toyota VVerso")
                .price(200)
                .state(ListingState.published)
                .dealerEntity(dealerEntity)
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build();

        when(listingRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(listingEntity));
        listingService.unPublishListing(listingId);

        assertThat(listingEntity).isNotNull();
        assertThat(listingEntity.getState()).isEqualTo(ListingState.draft);
    }

    @Test
    public void getListingEntityTest() throws ResourceNotfoundException {
        long listingId = 1;
        ListingEntity listingEntity = ListingEntity.builder()
                .id(listingId)
                .vehicle("Toyota VVerso")
                .price(200)
                .state(ListingState.draft)
                .dealerEntity(dealerEntity)
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build();
        when(listingRepository.findById(Mockito.any(Long.class))).thenReturn(Optional.ofNullable(listingEntity));
        ListingEntity listingEntityFound = listingService.getListingEntity(listingId);
        assertThat(listingEntityFound).isNotNull();
    }

}
