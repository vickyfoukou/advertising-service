package com.dfv.advertising.services;


import com.dfv.advertising.entities.DealerEntity;
import com.dfv.advertising.exceptions.ResourceNotfoundException;
import com.dfv.advertising.repositories.DealerRepository;
import com.dfv.advertising.serviceimpl.DealerServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DealerServiceTest {

    @Mock
    private DealerRepository dealerRepository;

    private DealerEntity dealerEntity;

    @InjectMocks
    private DealerServiceImpl dealerService;

    @BeforeEach
    public void setup(){
        dealerEntity = DealerEntity.builder()
                .id(1L)
                .name("Vicky Foukou")
                .tierLimit(2)
                .createdAt(LocalDateTime.now()).build();
    }

    @Test
    public void getDealerEntityTest() throws ResourceNotfoundException {
        when(dealerRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(dealerEntity));
        DealerEntity dealerEntityFound = dealerService.getDealerEntity(1L);
        assertThat(dealerEntityFound).isNotNull();
    }
}
