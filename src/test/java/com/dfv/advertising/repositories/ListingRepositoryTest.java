package com.dfv.advertising.repositories;

import com.dfv.advertising.commons.ListingState;
import com.dfv.advertising.entities.DealerEntity;
import com.dfv.advertising.entities.ListingEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class ListingRepositoryTest {

    @Autowired
    private ListingRepository listingRepository;

    @Autowired
    private DealerRepository dealerRepository;

    private DealerEntity dealerEntity;

    @BeforeEach
    public void setup(){
        dealerEntity = DealerEntity.builder()
                .name("Vicky Foukou")
                .tierLimit(2)
                .createdAt(LocalDateTime.now()).build();

        ListingEntity listingEntity_1 = ListingEntity
                .builder()
                .price(200)
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .vehicle("Toyota Verso")
                .state(ListingState.published).build();

        ListingEntity listingEntity_2 = ListingEntity
                .builder()
                .price(100)
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .vehicle("Toyota Verso")
                .state(ListingState.draft).build();

        ListingEntity listingEntity_3 = ListingEntity
                .builder()
                .price(100)
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .vehicle("Toyota carina")
                .state(ListingState.draft).build();


        dealerRepository.save(dealerEntity);
        listingEntity_1.setDealerEntity(dealerEntity);
        listingEntity_2.setDealerEntity(dealerEntity);
        listingEntity_3.setDealerEntity(dealerEntity);

        List<ListingEntity> listingEntityList = new ArrayList<>();
        listingEntityList.add(listingEntity_1);
        listingEntityList.add(listingEntity_2);
        listingEntityList.add(listingEntity_3);
        listingRepository.saveAll(listingEntityList);
    }

    @Test
    public void findAllListingPublishedByDealerTest(){
        Page<ListingEntity> entities = listingRepository.findAllListing(dealerEntity.getId(), ListingState.published, PageRequest.of(0, 10));
        assertThat(entities.getTotalElements()).isEqualTo(1);
    }


    @Test
    public void findAllListingUnPublishedByDealerTest(){
        Page<ListingEntity> entities = listingRepository.findAllListing(dealerEntity.getId(), ListingState.draft, PageRequest.of(0, 10));
        assertThat(entities.getTotalElements()).isEqualTo(2);
    }

    @Test
    public void getTotalOfPublishedListingsTest(){
        long total = listingRepository.getTotalOfPublishedListings(dealerEntity.getId());
        assertThat(total).isEqualTo(1);
    }

    @Test
    public void findFirstByDealerEntityAndStateOrderByCreatedAtAscTest(){
        ListingEntity oldestListing = listingRepository.findFirstByDealerEntityAndStateOrderByCreatedAtAsc(dealerEntity, ListingState.published);
        assertThat(oldestListing).isNotNull();
    }
}
