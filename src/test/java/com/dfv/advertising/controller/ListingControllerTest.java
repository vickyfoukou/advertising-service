package com.dfv.advertising.controller;

import com.dfv.advertising.commons.ListingState;
import com.dfv.advertising.dto.request.ActionDtoRequest;
import com.dfv.advertising.dto.request.ListingDtoRequest;
import com.dfv.advertising.entities.DealerEntity;
import com.dfv.advertising.entities.ListingEntity;
import com.dfv.advertising.mappers.ListingMapperImpl;
import com.dfv.advertising.repositories.ListingRepository;
import com.dfv.advertising.serviceimpl.ListingServiceImpl;
import com.dfv.advertising.services.ListingService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ListingController.class)
@ExtendWith(MockitoExtension.class)
public class ListingControllerTest {

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private ListingServiceImpl listingService;

    @MockBean
    private ListingMapperImpl listingMapper;

    @MockBean
    private ListingRepository listingRepository;

    @Autowired
    private MockMvc mockMvc;

    private DealerEntity dealerEntity;

    @BeforeEach
    public void setup(){
        dealerEntity = DealerEntity.builder()
                .id(1L)
                .name("Vicky Foukou")
                .tierLimit(2)
                .createdAt(LocalDateTime.now()).build();
    }


    @Test
    public void createListingTest() throws Exception {
        ListingDtoRequest listingDtoRequest = new ListingDtoRequest();
        listingDtoRequest.setDealerId(1);
        listingDtoRequest.setVehicle("Toyota Verso");
        listingDtoRequest.setPrice(100);

        String response = mockMvc.perform(
                post("/listings")
                .content(mapper.writeValueAsString(listingDtoRequest))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertThat(response).contains("New listing successfully created");
    }

    @Test
    public void updateListingTest() throws Exception {
        ListingDtoRequest listingDtoRequest = new ListingDtoRequest();
        listingDtoRequest.setDealerId(1);
        listingDtoRequest.setVehicle("Toyota Carina");
        listingDtoRequest.setPrice(350);

        String response = mockMvc.perform(
                put(String.format("/listings/%s", 1))
                        .content(mapper.writeValueAsString(listingDtoRequest))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertThat(response).contains("Listing successfully updated");
    }

    @Test
    public void publishListingTest() throws Exception {
        when(listingService.publishListing(ArgumentMatchers.anyLong(), ArgumentMatchers.any(ActionDtoRequest.class))).thenReturn(true);
        String response = mockMvc.perform(
                patch(String.format("/listings/%s/publish", 1))
                        .content(mapper.writeValueAsString(new ActionDtoRequest()))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertThat(response).contains("Listing successfully published");
    }

    @Test
    public void unPublishListingTest() throws Exception {
        String response = mockMvc.perform(
                patch(String.format("/listings/%s/unPublish", 1))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertThat(response).contains("Listing successfully unpublished");
    }

}
