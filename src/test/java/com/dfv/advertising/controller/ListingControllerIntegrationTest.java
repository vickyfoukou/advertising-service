package com.dfv.advertising.controller;

import com.dfv.advertising.dto.request.ActionDtoRequest;
import com.dfv.advertising.dto.request.ListingDtoRequest;
import com.dfv.advertising.dto.response.ListingDtoResponse;
import com.dfv.advertising.entities.ListingEntity;
import com.dfv.advertising.exceptions.ApiResponse;
import com.dfv.advertising.repositories.ListingRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.jdbc.Sql;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ListingControllerIntegrationTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private ListingRepository listingRepository;

    @BeforeEach
    public void setup() {
        testRestTemplate.getRestTemplate().setRequestFactory(new HttpComponentsClientHttpRequestFactory());
    }


    @Test
    public void createListingIntegrationTest(){
        ListingDtoRequest listingDtoRequest = new ListingDtoRequest();
        listingDtoRequest.setPrice(200);
        listingDtoRequest.setVehicle("Toyota Verso");
        listingDtoRequest.setDealerId(1);

        ResponseEntity<ApiResponse> response = testRestTemplate
                .postForEntity("/listings", listingDtoRequest, ApiResponse.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().isStatus()).isEqualTo(true);
        assertThat(response.getBody().getData()).isNotNull();
    }

    @Test
    @Sql("/test-data.sql")
    public void updateListingIntegrationTest(){
        ListingDtoRequest listingDtoRequest = new ListingDtoRequest();
        listingDtoRequest.setPrice(250);
        listingDtoRequest.setVehicle("Toyota Verso 2015");
        listingDtoRequest.setDealerId(1);

        testRestTemplate.put("/listings/{id}", listingDtoRequest, 1);

        ListingEntity listingEntity = listingRepository.findById(1L).orElse(null);
        assertThat(listingEntity).isNotNull();
        assertThat(listingEntity.getPrice()).isEqualTo(listingDtoRequest.getPrice());
        assertThat(listingEntity.getVehicle()).isEqualTo(listingDtoRequest.getVehicle());
    }

    @Test
    @Sql("/test-data.sql")
    public void publishListingTest() {
        ApiResponse apiResponse =  testRestTemplate.patchForObject("/listings/{id}/publish",
                new ActionDtoRequest(), ApiResponse.class, 1);
        assertThat(apiResponse.isStatus()).isEqualTo(true);
        assertThat(apiResponse.getMessage()).isEqualTo("Listing successfully published");
    }

    @Test
    @Sql("/test-data.sql")
    public void unPublishListingTest() {
        ApiResponse apiResponse =  testRestTemplate.patchForObject("/listings/{id}/unPublish",
                new ActionDtoRequest(), ApiResponse.class, 3);
        assertThat(apiResponse.isStatus()).isEqualTo(true);
        assertThat(apiResponse.getMessage()).isEqualTo("Listing successfully unpublished");
    }
}
