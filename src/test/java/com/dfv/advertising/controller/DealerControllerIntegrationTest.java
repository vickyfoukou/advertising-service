package com.dfv.advertising.controller;

import com.dfv.advertising.commons.ListingState;
import com.dfv.advertising.exceptions.ApiResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DealerControllerIntegrationTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    @Sql("/test-data.sql")
    public void getAllListingIntegrationTest(){
       ResponseEntity<ApiResponse> response =  testRestTemplate.getForEntity(String.format("/dealers/%s/listings?state=%s&page=%s&size=%s", 1,
               ListingState.draft, 1, 10), ApiResponse.class);

       assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
       assertThat(response.getBody()).isNotNull();
       assertThat(response.getBody().isStatus()).isEqualTo(true);
    }
}
