package com.dfv.advertising.controller;


import com.dfv.advertising.commons.ListingState;
import com.dfv.advertising.entities.DealerEntity;
import com.dfv.advertising.entities.ListingEntity;
import com.dfv.advertising.mappers.ListingMapperImpl;
import com.dfv.advertising.serviceimpl.ListingServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DealerController.class)
@ExtendWith(MockitoExtension.class)
public class DealerControllerTest {

    @MockBean
    private ListingServiceImpl listingService;

    @MockBean
    private ListingMapperImpl listingMapper;

    @Autowired
    private MockMvc mockMvc;

    private DealerEntity dealerEntity;

    @BeforeEach
    public void setup(){
        dealerEntity = DealerEntity.builder()
                .id(1L)
                .name("Vicky Foukou")
                .tierLimit(2)
                .createdAt(LocalDateTime.now()).build();
    }


    @Test
    public void getAllListingTest() throws Exception {
        List<ListingEntity> entities = new ArrayList<>();
        entities.add(ListingEntity.builder().price(200).vehicle("Toyota Verso").state(ListingState.draft).dealerEntity(dealerEntity).build());
        entities.add(ListingEntity.builder().price(100).vehicle("Toyota Carina").state(ListingState.draft).dealerEntity(dealerEntity).build());
        int size = 10;

        Page<ListingEntity> listingEntityPage = new PageImpl<>(entities, PageRequest.of(0, size), entities.size());

        when(listingService.getAllListing(ArgumentMatchers.anyLong(), ArgumentMatchers.any(ListingState.class),
                ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt())).thenReturn(listingEntityPage);

        String response = mockMvc.perform(
                get(String.format("/dealers/%s/listings/?state=%s", dealerEntity.getId(), ListingState.draft))
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertThat(response).contains("Listings loaded");
    }

}
