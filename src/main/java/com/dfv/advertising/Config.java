package com.dfv.advertising;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Bean
    public OpenAPI openApi() {
        return new OpenAPI()
                .info(new Info()
                        .title("Advertising Service")
                        .description("Rest API service for publishing cars online")
                        .version("v1.0")
                        .contact(new Contact()
                                .name("Vicky Djoulako")
                                .email("vikcy.foukou@gmail.com"))
                );
    }
}
