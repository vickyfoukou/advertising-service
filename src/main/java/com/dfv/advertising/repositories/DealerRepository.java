package com.dfv.advertising.repositories;

import com.dfv.advertising.entities.DealerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DealerRepository extends JpaRepository<DealerEntity, Long> {
}
