package com.dfv.advertising.repositories;

import com.dfv.advertising.commons.ListingState;
import com.dfv.advertising.entities.DealerEntity;
import com.dfv.advertising.entities.ListingEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ListingRepository extends JpaRepository<ListingEntity, Long> {

    @Query("select l from ListingEntity l where  l.dealerEntity.id = :dealerId and l.state = :state")
    Page<ListingEntity> findAllListing(@Param("dealerId") long dealerId, @Param("state") ListingState state, Pageable pageable);

    @Query("select  count(l.id) from ListingEntity l where l.dealerEntity.id = :dealerId and l.state = 'published' ")
    Long getTotalOfPublishedListings(@Param("dealerId") long dealerId);

    ListingEntity findFirstByDealerEntityAndStateOrderByCreatedAtAsc(DealerEntity dealerEntity, ListingState state);
}
