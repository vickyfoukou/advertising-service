package com.dfv.advertising.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class ListingDtoRequest {

    @NotNull
    @NotEmpty
    @Schema(description = "the name of the car", required = true)
    private String vehicle;

    @NotNull
    @Positive
    @Schema(description = "the price of the car", required = true)
    private float price;

    @NotNull
    @Positive
    @Schema(description = "The ID of dealer", required = true)
    private long dealerId;

}
