package com.dfv.advertising.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class ActionDtoRequest {
    @Schema(required = true, defaultValue = "false", allowableValues = {"false", "true"}, description = "set this attribute to true, to force the system publish the listing in the case the tier limit is reached")
    private boolean force = false;
}
