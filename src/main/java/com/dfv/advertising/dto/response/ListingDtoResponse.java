package com.dfv.advertising.dto.response;

import com.dfv.advertising.commons.ListingState;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class ListingDtoResponse {

    @Schema(description = "the ID of the listing")
    private long id;

    @Schema(description = "the name of listing")
    private String vehicle;

    @Schema(description = "the price of the listing")
    private float price;

    @Schema(description = "The state of the listing")
    private ListingState state;

    @Schema(description = "The date at the listing has been created")
    private String createdAt;
}
