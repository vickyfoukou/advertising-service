package com.dfv.advertising.serviceimpl;

import com.dfv.advertising.commons.ListingState;
import com.dfv.advertising.dto.request.ActionDtoRequest;
import com.dfv.advertising.dto.request.ListingDtoRequest;
import com.dfv.advertising.entities.DealerEntity;
import com.dfv.advertising.entities.ListingEntity;
import com.dfv.advertising.exceptions.ResourceNotfoundException;
import com.dfv.advertising.mappers.ListingMapper;
import com.dfv.advertising.repositories.ListingRepository;
import com.dfv.advertising.services.DealerService;
import com.dfv.advertising.services.ListingService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
@Slf4j
public class ListingServiceImpl implements ListingService {

    private final ListingRepository listingRepository;
    private final DealerService dealerService;
    private final ListingMapper mapper;

    @Override
    public Page<ListingEntity> getAllListing(long dealerId, ListingState state, int page, int size) {
        return listingRepository.findAllListing(dealerId, state, PageRequest.of(Math.max(page, 1) -1, size));
    }

    @Override
    public ListingEntity createListing(ListingDtoRequest listingDTO) throws ResourceNotfoundException {
        ListingEntity listingEntity = mapper.dtoToEntity(listingDTO);
        DealerEntity dealerEntity = dealerService.getDealerEntity(listingDTO.getDealerId());
        listingEntity.setDealerEntity(dealerEntity);
        listingRepository.save(listingEntity);
        log.info("New listing added by dealer with ID {}", listingEntity.getDealerEntity().getId());
        return listingEntity;
    }

    @Override
    public ListingEntity update(long listingId, ListingDtoRequest listingDTO) throws ResourceNotfoundException {
        ListingEntity listingEntity = listingRepository.findById(listingId).orElseThrow(
                () -> new ResourceNotfoundException("Listing not found with Id = " + listingId)
        );
        listingEntity.setPrice(listingDTO.getPrice());
        listingEntity.setVehicle(listingDTO.getVehicle());
        listingRepository.save(listingEntity);
        log.info("Listing with ID {} successfully updated", listingId);
        return listingEntity;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean publishListing(long listingId, ActionDtoRequest actionDtoRequest) throws ResourceNotfoundException {
        ListingEntity listingEntity = getListingEntity(listingId);

        if(ListingState.published.equals(listingEntity.getState())){
            log.warn("Listing with ID {} already published", listingId);
            return true;
        }

        DealerEntity dealerEntity = listingEntity.getDealerEntity();

        long totalOfPublishedListings = listingRepository.getTotalOfPublishedListings(dealerEntity.getId());
        log.info("total of published listing is  : {}", totalOfPublishedListings);

        if(totalOfPublishedListings < dealerEntity.getTierLimit() || actionDtoRequest.isForce()){
            if(totalOfPublishedListings == dealerEntity.getTierLimit()){
                ListingEntity oldestListingEntity = listingRepository.findFirstByDealerEntityAndStateOrderByCreatedAtAsc(dealerEntity, ListingState.published);
                oldestListingEntity.setState(ListingState.draft);
                listingRepository.save(oldestListingEntity);
                log.info("The oldest listing with ID {} has been unpublished", oldestListingEntity.getId());
            }
            listingEntity.setState(ListingState.published);
            listingRepository.save(listingEntity);
            log.info("The listing with ID {} successfully published ", listingId);
            return true;

        } else {
            log.info("Listing can not be published. Tier limit has been reached by the dealer");
            return false;
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void unPublishListing(long listingId) throws ResourceNotfoundException {
        ListingEntity entity = getListingEntity(listingId);
        if(!ListingState.draft.equals(entity.getState())){
            entity.setState(ListingState.draft);
            listingRepository.save(entity);
            log.info("Listing with ID {} successfully unpublished", listingId);
        } else{
            log.warn("Listing with ID {} already unpublished", listingId);
        }
    }

    public ListingEntity getListingEntity(long listing) throws ResourceNotfoundException {
        return listingRepository.findById(listing).orElseThrow(
                () -> new ResourceNotfoundException("Listing not found with Id = " + listing)
        );
    }

}
