package com.dfv.advertising.serviceimpl;

import com.dfv.advertising.entities.DealerEntity;
import com.dfv.advertising.exceptions.ResourceNotfoundException;
import com.dfv.advertising.repositories.DealerRepository;
import com.dfv.advertising.services.DealerService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class DealerServiceImpl implements DealerService {

    private final DealerRepository dealerRepository;

    @Override
    public DealerEntity getDealerEntity(long dealerId) throws ResourceNotfoundException {
        return dealerRepository.findById(dealerId).orElseThrow(
                () -> new ResourceNotfoundException("Dealer not found with Id = " + dealerId)
        );
    }
}
