package com.dfv.advertising.controller;


import com.dfv.advertising.dto.request.ActionDtoRequest;
import com.dfv.advertising.dto.request.ListingDtoRequest;
import com.dfv.advertising.dto.response.ListingDtoResponse;
import com.dfv.advertising.entities.ListingEntity;
import com.dfv.advertising.exceptions.ApiResponse;
import com.dfv.advertising.exceptions.ResourceNotfoundException;
import com.dfv.advertising.mappers.ListingMapper;
import com.dfv.advertising.services.ListingService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Tag(description = "Listing Resource that provide endpoints to manage listings", name = "Listing Resource")
@RestController
@RequestMapping(value = "/listings")
@AllArgsConstructor
public class ListingController {

    private final ListingService listingService;
    private final ListingMapper mapper;

    @Operation(summary = "Create listing", description = "create a listing", method = "POST")
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "200", description = "Action successfully done"),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content(schema =  @Schema(ref = "#/components/schemas/ApiResponseString"))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(schema =  @Schema(ref = "#/components/schemas/ApiResponseString"))
            )
    })
    @PostMapping
    public ResponseEntity<ApiResponse<ListingDtoResponse>> createListing(@RequestBody @Valid ListingDtoRequest listingDtoRequest) throws ResourceNotfoundException {
        ListingEntity listingEntity = listingService.createListing(listingDtoRequest);
        return new ResponseEntity<>(
                new ApiResponse<>(true, "New listing successfully created", mapper.entityToDto(listingEntity))
                , HttpStatus.OK);
    }

    @Operation(summary = "Update listing", description = "update an existing listing", method = "PUT")
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "200", description = "Action successfully done"),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content(schema =  @Schema(ref = "#/components/schemas/ApiResponseString"))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(schema =  @Schema(ref = "#/components/schemas/ApiResponseString"))
            )
    })
    @PutMapping(value = "/{listingId}")
    public ResponseEntity<ApiResponse<ListingDtoResponse>> updateListing(@RequestBody @Valid ListingDtoRequest listingDtoRequest,
                                                             @PathVariable long listingId) throws ResourceNotfoundException {
        ListingEntity listingEntity = listingService.update(listingId, listingDtoRequest);
        return new ResponseEntity<>(
                new ApiResponse<>(true, "Listing successfully updated", mapper.entityToDto(listingEntity))
                , HttpStatus.OK);
    }

    @Operation(summary = "Publish listing", description = "Publish a listing", method = "PUT")
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "200", description = "Action successfully done"),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content(schema =  @Schema(ref = "#/components/schemas/ApiResponseString"))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(schema =  @Schema(ref = "#/components/schemas/ApiResponseString"))
            )
    })
    @PatchMapping(value = "/{listingId}/publish")
    public ResponseEntity<ApiResponse<String>> publishListing(@RequestBody @Valid ActionDtoRequest actionDtoRequest,
                                                             @PathVariable long listingId) throws ResourceNotfoundException {
        if(listingService.publishListing(listingId, actionDtoRequest)){
            return new ResponseEntity<>(
                    new ApiResponse<>(true, "Listing successfully published", null)
                    , HttpStatus.OK);
        } else {
            return new ResponseEntity<>(
                    new ApiResponse<>(false, "Tier limit already reached", null)
                    , HttpStatus.BAD_REQUEST);
        }

    }

    @Operation(summary = "Unpublish Listing", description = "Unpublish a listing", method = "PUT")
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "200", description = "Action successfully done"),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content(schema =  @Schema(ref = "#/components/schemas/ApiResponseString"))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(schema =  @Schema(ref = "#/components/schemas/ApiResponseString"))
            )
    })
    @PatchMapping(value = "/{listingId}/unPublish")
    public ResponseEntity<ApiResponse<String>> UnPublishListing(@PathVariable long listingId) throws ResourceNotfoundException {
        listingService.unPublishListing(listingId);
        return new ResponseEntity<>(
                new ApiResponse<>(true, "Listing successfully unpublished", null)
                , HttpStatus.OK);
    }
}
