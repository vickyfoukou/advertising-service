package com.dfv.advertising.controller;


import com.dfv.advertising.commons.ListingState;
import com.dfv.advertising.commons.Pagination;
import com.dfv.advertising.dto.response.ListingDtoResponse;
import com.dfv.advertising.entities.ListingEntity;
import com.dfv.advertising.exceptions.ApiResponse;
import com.dfv.advertising.mappers.ListingMapper;
import com.dfv.advertising.services.ListingService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Tag(description = "Dealer Resource", name = "Dealer Resource")
@RestController
@RequestMapping(value = "/dealers")
@AllArgsConstructor
public class DealerController {

    private final ListingService listingService;
    private final ListingMapper mapper;

    @Operation(summary = "Get all Listings", description = "Get all listings of a dealer with a given state", method = "GET")
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "200", description = "Action successfully done"),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content(schema =  @Schema(ref = "#/components/schemas/ApiResponseString"))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(schema =  @Schema(ref = "#/components/schemas/ApiResponseString"))
            )
    })
    @GetMapping(value = "/{dealerId}/listings")
    public ResponseEntity<ApiResponse<Pagination<ListingDtoResponse>>> getAllListing(@RequestParam(required = false, defaultValue = "1") int page,
                                                                                     @RequestParam(required = false, defaultValue = "20") int size,
                                                                                     @PathVariable long dealerId,
                                                                                     @RequestParam(defaultValue = "draft") ListingState state) {

        Page<ListingEntity> listingEntityPage =  listingService.getAllListing(dealerId, state, page, size);

        List<ListingDtoResponse> listingDtoResponseList =  listingEntityPage.stream()
                .map(mapper::entityToDto)
                .collect(Collectors.toList());

        Pagination<ListingDtoResponse> listPagination = new Pagination<>(
                page, size, listingEntityPage.getTotalElements(), listingDtoResponseList);

        return new ResponseEntity<>(
                new ApiResponse<>(true,  "Listings loaded" , listPagination)
                , HttpStatus.OK);
    }
}
