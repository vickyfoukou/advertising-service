package com.dfv.advertising.mappers;

import com.dfv.advertising.dto.request.ListingDtoRequest;
import com.dfv.advertising.dto.response.ListingDtoResponse;
import com.dfv.advertising.entities.ListingEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ListingMapper {

    Class<? extends ListingMapper> INSTANCE = Mappers.getMapperClass(ListingMapper.class);

    ListingEntity dtoToEntity(ListingDtoRequest listingDto);

    ListingDtoResponse entityToDto(ListingEntity entity);
}
