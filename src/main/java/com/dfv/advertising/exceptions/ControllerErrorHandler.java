package com.dfv.advertising.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@ControllerAdvice
@Slf4j
public class ControllerErrorHandler {

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ApiResponse<String>> handleGlobalException(Exception e){
        e.printStackTrace();
        log.error(e.getMessage());
        return  new ResponseEntity<>(new ApiResponse<>(
                false,
                "Something went wrong. Please try again", null),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<ApiResponse<String>> handleMissingArguments(MissingServletRequestParameterException e){
        return  new ResponseEntity<>(new ApiResponse<>(
                false,
                e.getMessage(), null),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = { ResourceNotfoundException.class,})
    public ResponseEntity<ApiResponse<String>> handleOtherException(ResourceNotfoundException e){
        log.error(e.getMessage());
        return  new ResponseEntity<>(new ApiResponse<>(false, e.getMessage(), ""), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    protected ResponseEntity<ApiResponse<String>> handleMethodArgumentNotValid(MethodArgumentNotValidException e) {
        List<String> errors = new ArrayList<>();
        e.getBindingResult().getFieldErrors().
                forEach(fieldError -> errors.add(fieldError.getField().concat(": ")
                        .concat(Objects.requireNonNull(fieldError.getDefaultMessage()))));

        String message = String.join(", ", errors);
        log.error(message);
        return  new ResponseEntity<>(new ApiResponse<>(false, message, null), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({MethodArgumentTypeMismatchException.class, IllegalArgumentException.class})
    public ResponseEntity<ApiResponse<String>> handleIllegalArgumentException(Exception e){
        log.error(e.getMessage());
        return  new ResponseEntity<>(new ApiResponse<>(false, "arguments incorrect or missing", null), HttpStatus.BAD_REQUEST);
    }
}
