package com.dfv.advertising;

import com.dfv.advertising.commons.ListingState;
import com.dfv.advertising.entities.DealerEntity;
import com.dfv.advertising.entities.ListingEntity;
import com.dfv.advertising.repositories.DealerRepository;
import com.dfv.advertising.repositories.ListingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DataLoader {

    private final DealerRepository dealerRepository;
    private final ListingRepository listingRepository;

    @Autowired
    public DataLoader(DealerRepository dealerRepository, ListingRepository listingRepository){
        this.dealerRepository = dealerRepository;
        this.listingRepository = listingRepository;
        addDefaultDealer();
    }

    public void addDefaultDealer(){
        if(dealerRepository.findAll().isEmpty()){
            DealerEntity  dealerEntity = DealerEntity.builder().name("Vicky Djoulako").tierLimit(2).build();
            dealerRepository.save(dealerEntity);
            log.info("Default dealer added");

//            ListingEntity listingEntity1 = new ListingEntity();
//            listingEntity1.setVehicle("Toyota Verso 2015");
//            listingEntity1.setPrice(25000000);
//            listingEntity1.setDealerEntity(dealerEntity);
//
//            listingRepository.save(listingEntity1);
//
//            ListingEntity listingEntity2 = new ListingEntity();
//            listingEntity2.setVehicle("RAV 4 2020");
//            listingEntity2.setPrice(80000000);
//            listingEntity2.setDealerEntity(dealerEntity);
//            listingRepository.save(listingEntity2);
//
//            ListingEntity listingEntity3 = new ListingEntity();
//            listingEntity3.setVehicle("Mercedes c 2000");
//            listingEntity3.setPrice(4500000);
//            listingEntity3.setDealerEntity(dealerEntity);
//            listingRepository.save(listingEntity3);

        }
    }
}
