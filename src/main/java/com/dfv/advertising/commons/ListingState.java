package com.dfv.advertising.commons;

import javax.validation.Valid;

@Valid
public enum ListingState {
    published, draft;
}
