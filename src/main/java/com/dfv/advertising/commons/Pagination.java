package com.dfv.advertising.commons;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
public class Pagination <T>{
    private int page;
    private int size;
    private long total_of_items;
    private List<T> data;
}
