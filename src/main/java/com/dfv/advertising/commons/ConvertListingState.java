package com.dfv.advertising.commons;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ConvertListingState implements Converter<String, ListingState> {
    @Override
    public ListingState convert(String state) throws IllegalArgumentException {
        try {
            return ListingState.valueOf(state);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(("state is not allowed"));
        }
    }
}
