package com.dfv.advertising.services;

import com.dfv.advertising.entities.DealerEntity;
import com.dfv.advertising.exceptions.ResourceNotfoundException;

public interface DealerService {
    DealerEntity getDealerEntity(long dealerId) throws ResourceNotfoundException;
}
