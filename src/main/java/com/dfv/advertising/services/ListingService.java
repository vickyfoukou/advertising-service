package com.dfv.advertising.services;

import com.dfv.advertising.commons.ListingState;
import com.dfv.advertising.dto.request.ActionDtoRequest;
import com.dfv.advertising.dto.request.ListingDtoRequest;
import com.dfv.advertising.entities.ListingEntity;
import com.dfv.advertising.exceptions.ResourceNotfoundException;
import org.springframework.data.domain.Page;

public interface ListingService {
    Page<ListingEntity> getAllListing(long dealerId, ListingState state, int page, int size);
    ListingEntity createListing(ListingDtoRequest listingDTO) throws ResourceNotfoundException;
    ListingEntity update(long listingId, ListingDtoRequest listingDTO) throws ResourceNotfoundException;
    boolean publishListing(long listingId, ActionDtoRequest actionDtoRequest) throws ResourceNotfoundException;
    void unPublishListing(long listingId) throws ResourceNotfoundException;
}
